var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');
var cookieParser = require('cookie-parser');

var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var carnetCommande = require('../CarnetCommande').carnetCommande;

router.get('/', function(req, res) {
    res.sendFile("NavigationLivreur.html",{ root: path.join(__dirname, '../public/html/') });
});

router.get('/accepterCommande', function(req, res) {
    res.sendFile("accepterCommande.html",{ root: path.join(__dirname, '../public/html/') });
});

module.exports = router;
