var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');


var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var menuManager = require('../MenuManager').menuManager;
var restaurantManager = require('../RestaurantManager').restaurantManager;


/* GET home page. */
router.get('/', function(req, res) {

	
	if(req.session){
		if( req.session.user.type == "client"){
			var filepath = path.resolve("./public/html/NavigationClient.html")
			res.sendFile(filepath); 
			}
		else if( req.session.user.type == "restaurateur"){
			var filepath = path.resolve("./public/html/NavigationRestaurateur.html")
			res.sendFile(filepath); 
			} 
		else if( req.session.user.type == "entrepreneur"){
			var filepath = path.resolve("./public/html/NavigationEntrepreneur.html")
			res.sendFile(filepath); 
			} 
	}
	else{
		var filepath = path.resolve("./public/html/index.html")
		res.sendFile(filepath); 
		}
});

module.exports = router;
