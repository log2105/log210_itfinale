var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');
var cookieParser = require('cookie-parser');

var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var menuManager = require('../MenuManager').menuManager;
var restaurantManager = require('../RestaurantManager').restaurantManager;
var carnetCommande = require('../CarnetCommande').carnetCommande;
var user;


//envoyer la page pour la gestion des menus et des commandes
router.get('/menu', function(req, res) {
	res.sendFile("index.html",{ root: path.join(__dirname, './public/html/') });
});

//commande pour obtenir un menu
//prend en paramètre un nom de menu et un identifiant de restaurant
//sous la forme {restaurant: "identifiant du restaurant en int", nom: "nom du menu, en string"}
router.get('/getMenu', function(req, res) {
	menuManager.getMenu(req.restaurant, req.nom, function(menu){
        res.send(menu);
    })
});

//Prend un ID en entre et retounre
//la commande associe
router.post('/getCommande', function(req, res){ 
    var ID = req.body.ID;
    console.log(ID);
		databaseManager.getCommande(ID, function(result){
			res.send(result);
		});
});




//page pour changer le statut d'une commande
router.get('/changerStatut', function(req, res) {
	if(req.session.user){
		user = req.session.user;
console.log(user);
		res.render('changerStatutCommande',{nom: user.nom, date: user.dateNaissance, adresse: user.adresse, telephone: user.telephone, motDePasse: user.motDePasse, courriel: user.courriel});
	}
});

//Get pour envoyer la page de gestion de menu
router.get('/ajouterMenu', function(req, res) {	
	res.render('ajouterMenu');
});

//commande pour envoyer un menu
//Prend un menu en paramètre
router.post('/ajouterMenu', function(req, res) {
			menu = req.body;
			menuManager.ajouterMenu(menu, function(message){
				res.send({data:"Menu ajouté"});

		});

});

//retourne une liste de commandes pretes
router.get('/getCommandePrete', function(req, res){
		carnetCommande.getCommandePrete(function(result){
			res.send(result);
		});
});

//retourne une liste de commandes pas pretes
router.get('/getCommandePasPrete', function(req, res){
		carnetCommande.getCommandePasPrete(function(result){
			res.send(result);
		});
});

//retourne une liste de commandes pas pretes pour l'utilisateur
router.get('/getCommandePasPretePourRestaurateur', function(req, res){
console.log(user);
		carnetCommande.getCommandePasPretePourRestaurateur(user.courriel, function(result){
			res.send(result);
		});
});

//change le statut d'une commande
router.post('/changerStatutCommande', function(req, res){ 
    var ID = req.body.ID;
    console.log(parseInt(ID));
    var statut = req.body.statut;
		carnetCommande.changerStatutCommande(ID, statut, function(result){
			res.send(result);
		});
});

//met la date courante a dateLivraison d'une commande
router.post('/etampeCommande', function(req, res){ 
    var ID = req.body.ID;
    console.log(parseInt(ID));
		carnetCommande.etampeCommande(ID, function(){
			res.send();
		});
});

//commande pour modifier un menu
//Prend un menu en paramètre
router.post('/modifierMenu', function(req, res) {
    menu = req.body;
    menuManager.modifierMenu(menu, function(message){
        res.send({data:"Menu modifié"});

    });

});

//commande pour supprimer un menu
//Prend en paramètre un menu
router.post('/supprimerMenu', function(req, res) {
    menu = req.body;
    menuManager.supprimerMenu(menu.restaurant, menu.nom, function(message){
        res.send({data:"Menu modifié"});

    });
});

module.exports = router;
