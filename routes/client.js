var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');
var cookieParser = require('cookie-parser');

var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var menuManager = require('../MenuManager').menuManager;
var restaurantManager = require('../RestaurantManager').restaurantManager;
var userManager = require('../UserManager').userManager;
var carnetCommande = require('../CarnetCommande').carnetCommande;

var user;
var total=0;

router.get('/passerCommande', function(req, res) {
	user = req.session.user;	
console.log(user);
	res.render("passerCommande",{nom: user.nom, date: user.dateNaissance, adresse: user.adresse, telephone: user.telephone, motDePasse: user.motDePasse, courriel: user.courriel});
	
});

router.get('/payerCommande', function(req, res) {
	res.render('paiement', total);
});

//retourne une liste de menus
router.post('/getNomMenu', function(req, res) {
    menuManager.getNomMenu(req.body.restaurant, function(noms){
        res.send(noms);
    });

});

//inscrire la commande
router.post('/inscrireCommande', function(req, res) {
console.log(req.body);
total = req.body.total;
    carnetCommande.ajouterCommande(req.body, function(){
        res.send();
    });

});

//retourne une liste de plats selon le menu et restaurant
router.post('/getMenu', function(req, res) {
    menuManager.getMenu(req.body.restaurant, req.body.nom, function(plats){
        res.send(plats);
    });

});

//retourne un client
router.post('/getClient', function(req, res) {
    userManager.getInfo(user.courriel, function(client){
        res.send(client);
    });

});

//retourne un carnet d'adresse
router.post('/getCarnetAdresse', function(req, res) {
    userManager.getCarnetAdresse(req.body.ID, function(carnet){
        res.send(carnet);
    });

});

router.post('/changerAdresseDefaut', function(req, res){
	userManager.changerAdresseDefaut(req.body.ID, req.body.adresse, function(){
	});
});

module.exports = router;
