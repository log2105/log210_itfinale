var express = require('express');
var router = express.Router();
var app = express(); 
	
var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');
var cookieParser = require('cookie-parser');

var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var menuManager = require('../MenuManager').menuManager;
var restaurantManager = require('../RestaurantManager').restaurantManager;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

/* GET home page. */
router.get('/', function(req, res) {
	console.log("test");
	var filepath = path.resolve("./public/html/SignUpVisiteur.html");
	res.sendFile(filepath); 
	});

router.post('/', function(req, res) {
	
	console.log(req.body);
	userManager.inscrireUtilisateur(
		req.body.prenom, 
		req.body.nom, 
		req.body.dateNaissance, 
		req.body.adresse, 
		req.body.numeroTelephone, 
		req.body.motDePasse, 
		req.body.courriel,
		req.body.typeUtilisateur,
		function(user){
		
			if(!user){
				res.render("message",{message: "Échec d'authentification"});
			}
			else{
				req.session.user = user;
				res.render("userInfo",{message: "Votre compte a été enregistré!",nom: user.nom, adresse: user.adresse, telephone: user.telephone, date: user.dateNaissance, courriel: user.courriel});
			}	
		
	});
	
	});

module.exports = router;
