var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');
var cookieParser = require('cookie-parser');

var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var menuManager = require('../MenuManager').menuManager;
var restaurantManager = require('../RestaurantManager').restaurantManager;
var userManager = require('../UserManager').userManager;


//envoyer la page pour la gestion des restaurants
router.get('/restaurant', function(req, res) {
    res.sendFile("index.html",{ root: path.join(__dirname, './public/html/') });
});

//retourne la liste des restaurants sous la forme d'un tableau
// de restaurants
router.get('/getListeRestaurants', function(req, res) {
    restaurantManager.getListeRestaurant(function(liste){
        res.send(liste);
    })
});

//ajoute un restaurant
//prend en paramètre un restaurant
router.post('/ajouterRestaurant', function(req, res) {
    restaurant = req.body;
    console.log(restaurant);
    restaurantManager.ajouterRestaurant(restaurant, function(message){
        res.send(message);
    })

});

//change le restaurateur
//prend en paramètre un ID de restaurant et un courriel de restaurateur
router.post('/setRestaurateur', function(req, res) {
    ID = req.body.ID;
    restaurateur = req.body.Restaurateur;
    console.log(req.body);
    restaurantManager.changerRestaurateur(ID, restaurateur, function(message){
        res.send(message);
    })

});

//ajoute un restaurateur
//prend en paramètre un restaurateur
router.post('/ajouterRestaurateur', function(req, res) {
    console.log(req.body);

    prenom = req.body.prenom;
    nom = req.body.name;
    dateNaissance = req.body.dateNaissance;
    adresse = req.body.adresse;
    numeroTelephone = req.body.numeroTelephone;
    courriel = req.body.courriel;
    password = req.body.password;
    type = req.body.type;

    userManager.inscrireUtilisateur(prenom, nom, dateNaissance, adresse, numeroTelephone, password, courriel, type, function(message){
        res.send(message);
    })

});

//modifie un restaurant
//prend en paramètre un restaurant
router.post('/modifierRestaurant', function(req, res) {
    restaurant = req.body;
    restaurantManager.modifierRestaurant(restaurant, function(message){
        res.send(message);
    })

});

//retourne une liste de restaurateurs sous la forme d'un tableau de
//{nom: "le nom", courriel:"le courriel"}
router.get('/getListeRestaurateurs', function(req, res) {
    userManager.getListeRestaurateur(function(liste){
        res.send(liste);
    });

});

//retourne un restaurant
router.post('/getRestaurant', function(req, res) {
    restaurantManager.getRestaurant(req.body.ID, function(restaurant){
        res.send(restaurant);
    });

});

//retourne un restaurateur
router.post('/getRestaurateur', function(req, res) {
    userManager.getInfo(req.body.ID, function(restaurateur){
        res.send(restaurateur);
    });

});


/**
 * Route utilisée pour envoyer la page d'ajout de restaurant
 */
router.get('/ajouterRestaurant', function(req, res) {	
	res.render("ajouterRestaurant");
});

/**
 * Route utilisée pour envoyer la page d'ajout de restaurateur
 */
router.get('/ajouterRestaurateur', function(req, res) {	
	res.render("ajouterRestaurateur");
});

/**
 * Route utilisée pour envoyer la page de gestion de restaurant
 */
router.get('/gestionRestaurant', function(req, res) {	
	res.render("gestionRestaurant");
});

/**
 * Route utilisée pour envoyer la page de modification de restaurateur
 */
router.get('/gestionRestaurateur', function(req, res) {	
	res.render("gestionRestaurateur");
});

/**
* Prend la liste de restaurateurs et l'envoi a la page
* qui pour set un restaurateur au restaurant
*/
router.get('/setRestaurateur', function(req, res) {
	res.render("setRestaurateur");
});

//supprime un restaurant
//prend en paramètre un identifiant de restaurant (int)
router.post('/supprimerRestaurant', function(req, res) {
    var ID = req.body.ID;
    console.log(parseInt(ID));
    restaurantManager.supprimerRestaurant(parseInt(ID), function(message){
        res.send(message);
    })
});

//supprime un restaurateur
//prend en paramètre un identifiant de restaurateur (int)
router.post('/supprimerRestaurateur', function(req, res) {
    var ID = req.body.ID;
    console.log(parseInt(ID));
    userManager.supprimerUtilisateur(parseInt(ID), function(message){
        res.send(message);
    })
});

//modifie un restaurateur
//prend en paramètre un restaurateur
router.post('/modifierRestaurateur', function(req, res) {
    console.log(req.body);

    adresse = req.body.adresse;
    numeroTelephone = req.body.numeroTelephone;
    courriel = req.body.courriel;
    password = req.body.password;

    userManager.modifierUtilisateur(courriel, adresse, numeroTelephone, password, function(message){
        res.send(message);
    })

});

module.exports = router;
