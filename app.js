var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');

var login = require('./routes/login');
var gestionCompte = require('./routes/gestionCompte');
var signup = require('./routes/signup');
var restaurateur = require('./routes/restaurateur');
var admin = require('./routes/admin');
var livreur = require('./routes/livreur');
var client = require('./routes/client');

var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var databaseManager = require('./DatabaseManager');

var request = require('request');

var userManager = require('./UserManager').userManager;
var menuManager = require('./MenuManager').menuManager;
var restaurantManager = require('./RestaurantManager').restaurantManager;


var app = express();




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public/html')));
app.use(express.static(path.join(__dirname, 'public/html/images')));

app.use(session({
  secret: 'JeSuisUnSecret',
  resave: false,
  saveUninitialized: true
}));


app.use('/', routes);
app.use('/login', login);
app.use('/gestionCompte', gestionCompte);
app.use('/signup', signup);
app.use('/admin', admin);
app.use('/restaurateur', restaurateur);
app.use('/livreur', livreur);
app.use('/client', client);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.listen(3000,function(){
	console.log("Express is running on port 3000");
	});


module.exports = app;
