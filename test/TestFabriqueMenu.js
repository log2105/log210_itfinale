/**
 * Created by Frank on 2015-03-03.
 */
var assert = require('assert');
var restaurantManager = require("../RestaurantManager.js").restaurantManager;
var dataBaseManager = require("../DataBaseManager");
var menuManager = require("../MenuManager").menuManager;
var fabriqueMenu = require("../public/javascript/FabriqueMenu").FabriqueMenu;

describe('FabriqueMenu', function() {

    describe('creerMenu()', function () {
        it('fabrique à menu', function (done) {
           var monMenu = fabriqueMenu.creerMenu("Je suis un Test", "La patate à Michaud");
           monMenu.ajouterPlat("Pomme", "Une pomme", 5);
            monMenu.ajouterPlat("Poire", "Une poire", 6);
            monMenu.ajouterPlat("Patate", "Une patate", 4);
            assert.equal("Je suis un Test", monMenu.nom);
            assert.equal("La patate à Michaud", monMenu.restaurant);
            assert.equal("Pomme", monMenu.plat[0].nom);
            assert.equal("Poire", monMenu.plat[1].nom);
            assert.equal("Patate", monMenu.plat[2].nom);
            assert.equal(5, monMenu.plat[0].prix);
            assert.equal(6, monMenu.plat[1].prix);
            assert.equal(4, monMenu.plat[2].prix);
            done();
        });

    });


});