/**
 * Created by Frank on 2015-03-03.
 */

var assert = require('assert');
var restaurantManager = require("../RestaurantManager.js").restaurantManager;
var dataBaseManager = require("../DataBaseManager");
var menuManager = require("../MenuManager").menuManager;
var fabriqueUtilisateur = require("../public/javascript/FabriqueUtilisateur").FabriqueUtilisateur;

describe('FabriqueMenu', function() {

    describe('creerUtilisateur()', function () {
        it('fabrique à Utilisateur', function (done) {
            var monUtilisateur = fabriqueUtilisateur.creerUtilisateur("Steve A", "1/1/2001", "123 fausse rue", "438-312-3123", "gratex@ga", "admin", "client");

            assert.equal("Steve A", monUtilisateur.nom);
            assert.equal("1/1/2001", monUtilisateur.dateNaissance);
            assert.equal("123 fausse rue", monUtilisateur.adresse);
            assert.equal("438-312-3123", monUtilisateur.telephone);
            assert.equal("gratex@ga", monUtilisateur.courriel);
            assert.equal("admin", monUtilisateur.motDePasse);
            assert.equal("client", monUtilisateur.type);
            assert.equal("./public/html/NavigationHome.html", monUtilisateur.homePage);
            done();
        });

    });


});
