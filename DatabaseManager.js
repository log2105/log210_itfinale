var mysql = require('mysql');
var bdDonne;
var currentLogin;
var fabriqueCommande = require("./public/javascript/FabriqueCommande");
var fabriqueLivraison = require("./public/javascript/FabriqueLivraison").FabriqueLivraison;
var fabriqueUtilisateur = require("./public/javascript/FabriqueUtilisateur");


/**
 * Permet de se connecter à la base de donnée.
 * Le nom d'utilisateur et le mot de passe doivent être ajouté ici.
 * Retourne un objet de connexion à la BD.
 */
var connecterLaBD = function(){
	
	var connexion = mysql.createConnection({
        host	:'localhost',
        user	:'root',
        password:'RustInPiece', //mettez votre propre password obviously
        database:'mydb'
    });
	connexion.connect();
	return connexion;
	
};

/**
 * Modifie un utilisateur dans la BD.
 * Change les valeurs de adresse, telephone et mot de passe.
 * Courriel est utilisé comme indentifiant unique dans la BD.
 * Appele la fonction callback après que la requête SQL se soit terminé.
 */
var modifierUser = function(courriel, adresse, telephone, motDePasse, callback){

	var connexion = connecterLaBD();

	var post = {adresse: adresse, numeroTelephone: telephone, password: motDePasse};

	connexion.query("UPDATE users SET ? WHERE courriel ="+"'"+courriel+"'", post, function(err, rows, fields){
		if(err) throw err;
		callback();
		connexion.end();
		
	});

};

/**
 * Trouve les informations d'un utilisateurs dans la BD.
 * retourne un objet de type userInfo contenant les informations sur l'utilisateur dans la BD.
 * Appele la fonction callback après que la requête SQL se soit terminé.
 */

var getInfo = function(courriel, callback){
	var connexion = connecterLaBD();
	
	var query = "SELECT * FROM users WHERE courriel="+"'"+courriel+"'";
	
	connexion.query(query, function(err, rows, fields){
		
		if(err) throw err;
		
		if(rows.length > 1){
			throw err;
		}
		else{
            utilisateur = fabriqueUtilisateur.creerUtilisateur(rows[0].name, rows[0].dateNaissance, rows[0].adresse, rows[0].numeroTelephone, rows[0].courriel, rows[0].password, rows[0].compte, rows[0].carnetAdresse)
			callback(utilisateur);
		}
	
	connexion.end();
	});
};

/**
 * Ajoute un utilisateur dans la BD.
 * Appele la fonction callback après que la requête SQL se soit terminé.
 */
var inscrireUser = function(prenom, nom, dateNaissance, adresse, telephone, motDePasse, courriel, type, callback){
	var connexion = connecterLaBD();
	
	var bdNom = prenom+" "+nom;
	var bdDate = dateNaissance;
	var bdAdresse = adresse;
	var bdTelephone = telephone;
	var bdMotDePasse = motDePasse;
	var bdCourriel = courriel;
	var bdCompte = type;
	var post = {name: bdNom,dateNaissance: bdDate, adresse: bdAdresse, numeroTelephone: bdTelephone, courriel: bdCourriel, password: bdMotDePasse, compte: bdCompte };

	connexion.query('INSERT INTO users SET ?', post , function(err, rows, fields){
		if(err) throw err;
	callback();
	connexion.end();
	});	
	
};

//Supprime un user selon son user_id
var supprimerUser = function(ID, callback){

	var connexion = connecterLaBD();
	
	connexion.query("DELETE FROM users WHERE user_id="+"'"+ID+"'", function(err, rows, fields){
		if(err) throw err;
		callback("Utilisateur supprimé");
		connexion.end();
	
	});
	
};


var ajouterRestaurant = function(restaurant, callback){
	var connexion = connecterLaBD();
	
	var post = {Nom: restaurant.nom, Adresse: restaurant.adresse, Telephone: restaurant.telephone, Restaurateur: restaurant.restaurateur};
	
	connexion.query('INSERT INTO restaurant SET ?', post , function(err, rows, fields){
		if(err) throw err;
	callback("Restaurant ajouté");
	connexion.end();
	});
	
};
	
var modifierRestaurant = function(restaurant, callback){
	
	var connexion = connecterLaBD();
	
    var post = {Nom: restaurant.nom, Adresse: restaurant.adresse, Telephone: restaurant.telephone, Restaurateur: restaurant.restaurateur, ID: restaurant.ID};

    //connexion.query("UPDATE restaurant SET ? WHERE ID ="+restaurant.ID.toString(), post, function(err, rows, fields)

    connexion.query('UPDATE restaurant SET Nom = ?,Adresse = ?,Telephone = ?,Restaurateur = ? WHERE ID = ?', [restaurant.nom,restaurant.adresse,restaurant.telephone,restaurant.restaurateur,restaurant.ID], function(err, result) {
        if(err) throw err;
        callback("Restaurant modifié");
        connexion.end();
    });

   /*connexion.query('UPDATE restaurant SET Nom = ? WHERE ID = ?', [restaurant.nom, restaurant.ID.toString()])*/
    /*connexion.query("UPDATE restaurant SET ? WHERE courriel ="+"'"+courriel+"'", post, function(err, rows, fields){
        if(err) throw err;
        callback();
        connexion.end();

    });*/

};
	
var supprimerRestaurant = function(ID, callback){
	
	var connexion = connecterLaBD();
	
	connexion.query("DELETE FROM restaurant WHERE ID="+"'"+ID+"'", function(err, rows, fields){
		if(err) throw err;
		callback("Restaurant supprimé");
		connexion.end();
	
	});	
};
	
var getRestaurant = function(ID, callback){
	
	var connexion = connecterLaBD();	
	var query = "SELECT * FROM restaurant WHERE ID="+ID.toString();
	
	connexion.query(query, function(err, rows, fields){
		
		if(err) throw err;	
		if(rows.length > 1){
			throw err;
		}
		else{
		
			var restoInfo = {
					nom:rows[0].Nom,
				    adresse:rows[0].Adresse,
				    telephone:rows[0].Telephone,
				    restaurateur:rows[0].Restaurateur,
                    ID:rows[0].ID
				};
			
			
			callback(restoInfo);
		}
	
	connexion.end();
	});	
};

var getListeRestaurant = function(callback){

    var connexion = connecterLaBD();
    var query = "SELECT * FROM restaurant";

    connexion.query(query, function(err, rows, fields){

        if(err) throw err;
        if(rows.length < 1){
            throw err;
        }
        else{
            var listeResto = [];
            rows.forEach(function(entry) {
                var restoInfo = {
                    nom:entry.Nom,
                    adresse:entry.Adresse,
                    telephone:entry.Telephone,
                    restaurateur:entry.Restaurateur,
                    ID:entry.ID
                };
                listeResto.push(restoInfo);

            });



            callback(listeResto);
        }

        connexion.end();
    });

};

var getRestaurantID = function(restaurateur, callback){

		var connexion = connecterLaBD();	
		var query = "SELECT * FROM restaurant WHERE restaurateur="+"'"+restaurateur+"'";	
		
		connexion.query(query, function(err, rows, fields){
			
			if(err) throw err;
            var liste = [];

            rows.forEach(function(entry) {
                var listEntry = {
                    nom:entry.Nom,
                    adresse:entry.Adresse,
                    ID:entry.ID
                };
                liste.push(listEntry);

            });

			callback(liste);

		    connexion.end();
		});		
};
	
var ajouterMenu = function(menu, callback){

    nbPlat = menu.plat.length;
    ajouterPlat(nbPlat, nbPlat, menu.restaurant, menu, callback);
	
};

var ajouterPlat = function(nbPlat, platRestant, restaurant, menu, callback){
    if(platRestant == 0){
        callback();
    }
    else{
        var connexion = connecterLaBD();
        var i = nbPlat - platRestant;
        var post = {NomMenu: menu.nom, Restaurant : parseInt(restaurant), NomPlat: menu.plat[i].nom, DescriptionPlat: menu.plat[i].description, PrixPlat: parseFloat(menu.plat[i].prix)};
        connexion.query('INSERT INTO plat SET ?', post , function(err, rows, fields){
            if(err) throw err;
            platRestant = platRestant -1;
            ajouterPlat(nbPlat, platRestant, restaurant, menu, callback);
            connexion.end();

        });


    }
};
	 
var modifierMenu = function(menu, callback){
	supprimerMenu(menu.restaurant, menu.nom, function(){
		ajouterMenu(menu, callback)
		
	});
	
	
	
};

var supprimerMenu = function(restaurant, nom, callback){
	var connexion = connecterLaBD();

		var query = "DELETE FROM plat WHERE Restaurant="+parseInt(restaurant)+" AND NomMenu='"+nom+"'";
		connexion.query(query, function(err, rows, fields){
			if(err) throw err;
			
			callback("menu supprimé");
			connexion.end();
			 
		});
		
};

var getNomMenu = function(restaurant, callback){
	var connexion = connecterLaBD();
	var dernierNom;

	var query = "SELECT * FROM plat WHERE Restaurant="+"'"+restaurant+"'";
}


var getMenu = function(restaurant, nom, callback){
	var connexion = connecterLaBD();
	
	var query = "SELECT * FROM plat WHERE Restaurant="+"'"+restaurant+"' AND NomMenu='"+nom+"'";
	
	connexion.query(query, function(err, rows, fields){
		if(err) throw err;
console.log(rows);
		if(rows.length >= 1){
			var menuInfo = {
				nom:rows[0].NomMenu,
				plat:[]
				}; 
            rows.forEach(function(entry) {
                var platInfo = {
                    nom:entry.NomPlat,
                    description:entry.DescriptionPlat,
                    prix:entry.PrixPlat
                 };
                menuInfo.plat.push(platInfo);
                });
			callback(menuInfo);

        }
		else{
			callback();
		}	
	});
};

var getListeRestaurateur = function(callback){
	
	var connexion = connecterLaBD();
	var query = "SELECT courriel,name,user_id FROM users WHERE compte='restaurateur'";
	connexion.query(query, function(err, rows, fields){
        var listeRestaurateur = [];
        rows.forEach(function(entry){
            restaurateurInfo = {
                nom:entry.name,
                courriel:entry.courriel,
		user_id:entry.user_id
            };
            listeRestaurateur.push(restaurateurInfo)
        });
		callback(listeRestaurateur)
	});
};

var ajouterCommande = function(commande, callback){
    var connexion = connecterLaBD();
    var post = {restaurant: commande.restaurant, client: commande.client, adresseLivraison:commande.adresseLivraison, dateLivraison: commande.dateLivraison, menu: commande.menu, plats: commande.plats, status: "Accepté" };


    connexion.query('INSERT INTO commande SET ?', post , function(err, rows, fields){
        if(err) throw err;
        callback("Commande ajoutée");
        connexion.end();
    });

};

var getCommande = function(ID, callback){
    var connexion = connecterLaBD();
    var query = "SELECT * FROM commande WHERE ID='"+parseInt(ID)+"'";

    connexion.query(query, function(err, rows, fields){


        if(err) throw err;
        if(rows.length > 1){
            throw err;
        }
        else{
            var commande = fabriqueCommande.creerCommandeComplete(rows[0].ID, rows[0].restaurant, rows[0].client, rows[0].adresseLivraison, rows[0].menu, rows[0].plats, rows[0].status, rows[0].dateLivraison);
            callback(commande);
        }

        connexion.end();
    });
};

var changerStatusCommande = function(ID, status, callback){
    var connexion = connecterLaBD();
    connexion.query("UPDATE commande SET status = '"+status+"' WHERE ID='"+parseInt(ID)+"'", function(err, rows, fields){
        if(err) throw err;
        callback();
        connexion.end();

    });
};

var supprimerCommande = function(ID, callback){
    var connexion = connecterLaBD();
    var query = "DELETE FROM commande WHERE ID="+ID.toString();

    connexion.query(query, function(err, rows, fields){

        if(err) throw err;

        callback("commande supprimée");

        connexion.end();
    });
};

var getCommandePasPrete = function(callback){
    var connexion = connecterLaBD();
    var query = "SELECT * FROM commande WHERE NOT status='Prête' AND NOT status='En livraison'";

    connexion.query(query, function(err, rows, fields){

        if (err) {
            throw err
        }

        if (rows.length != 0) {
            var listeCommandes = [];
            rows.forEach(function (entry) {
                uneCommande = fabriqueCommande.creerCommandeComplete(entry.ID, entry.restaurant, entry.client, entry.adresseLivraison, entry.menu, JSON.parse(entry.plats), entry.status, entry.dateLivraison);
                listeCommandes.push(uneCommande);
            });
            callback(listeCommandes);


        } else {
            callback("Aucune commande trouvé");
        }
        connexion.end();

    });
};

var getCommandePasPretePourListeRestaurant = function(ListeID, callback){
    var connexion = connecterLaBD();

    var query = "SELECT * FROM commande WHERE NOT status='Prête' AND NOT status='En livraison' AND (";
    ListeID.forEach(function(entry){
        var subQuery = "restaurant = "+entry.ID+" OR "
        query = query + subQuery
    })
    query = query.slice(0,-3);
    query = query + ")";


    connexion.query(query, function(err, rows, fields){

        if (err) {
            throw err
        }

        if (rows.length != 0) {
            var listeCommandes = [];
            rows.forEach(function (entry) {

                uneCommande = fabriqueCommande.creerCommandeComplete(entry.ID, entry.restaurant, entry.client, entry.adresseLivraison, entry.menu, entry.plats, entry.status, entry.dateLivraison);
                listeCommandes.push(uneCommande);
            });
            callback(listeCommandes);


        } else {
            callback("Aucune commande trouvé");
        }
        connexion.end();

    });
};

var getCommandePrete = function(callback){
    var connexion = connecterLaBD();
    var query = "SELECT * FROM commande WHERE status='Prête'";

    connexion.query(query, function(err, rows, fields){

        if (err) {
            throw err
        }

        if (rows.length != 0) {
            var listeCommandes = [];
            rows.forEach(function (entry) {
                uneCommande = fabriqueCommande.creerCommandeComplete(entry.ID, entry.restaurant, entry.client, entry.adresseLivraison, entry.menu, entry.plats, entry.status, entry.dateLivraison);
                listeCommandes.push(uneCommande);
            });
            callback(listeCommandes);


        } else {
            callback("Aucune commande trouvé");
        }
        connexion.end();

    });
};

//met la date dans le champs dateLivraison d'une commande
var etampeCommande = function(ID, callback){
    var connexion = connecterLaBD();

    connexion.query("UPDATE commande SET dateLivraison = CURRENT_TIMESTAMP WHERE ID='"+parseInt(ID)+"'", function(err, rows, fields){
        if(err) throw err;
        callback();
        connexion.end();

    });
}

//parcours la liste des plats et retourne les differents
//noms de menu
var getNomMenu = function (restaurant, callback){
	var connexion = connecterLaBD();
	var dernierNom='';
	var noms=[];
	var query = "SELECT * FROM plat WHERE restaurant='"+restaurant+"'";

    connexion.query(query, function(err, rows, fields){
		if (err) {
		    throw err
		}

		if (rows.length != 0) {
			
		    rows.forEach(function (entry) {
					if(dernierNom==''){
						noms.push(entry.NomMenu);
						dernierNom = entry.NomMenu;
					}else if(dernierNom != entry.NomMenu){
						noms.push(entry.NomMenu);
						dernierNom = entry.NomMenu;
					}
		    });
		    callback(noms);


		} else {
		    callback("Aucun menu trouvé");
		}
		connexion.end();
    });
	
}

var trouverLivraison = function(ID, callback){

    var connexion = connecterLaBD();
    var query = "SELECT * FROM livraison WHERE ID="+ID.toString();

    connexion.query(query, function(err, rows, fields){

        if(err) throw err;
        if(rows.length > 1){
            throw err;
        }
        else{
            uneLivraison = fabriqueLivraison.creerLivraisonAvecDate(rows[0].ID, rows[0].commande, rows[0].livreur, rows[0].dateDepart);
            callback(uneLivraison);
        }

        connexion.end();
    });

};

var inscrireLivraison = function(uneLivraison, callback){
    var connexion = connecterLaBD();

    var post = {commande: uneLivraison.commande, livreur: uneLivraison.livreur, dateDepart: dateComplete };

    connexion.query('INSERT INTO livraison SET ?', post , function(err, rows, fields){
        if(err) throw err;
        callback("Livraison inscrite");
        connexion.end();

    });

};

var trouverLivraisonPourLivreur = function(livreur, callback){

    var connexion = connecterLaBD();
    var query = "SELECT * FROM livraison WHERE livreur='"+livreur.toString()+"'";

    connexion.query(query, function(err, rows, fields){

        if(err) throw err;
        tableauLivraison = []
        for(i=0;i<rows.length;i++){
            uneLivraison = fabriqueLivraison.creerLivraisonAvecDate(rows[i].ID, rows[i].commande, rows[i].livreur, rows[i].dateDepart);
            tableauLivraison.push(uneLivraison)
        }

        callback(tableauLivraison);


        connexion.end();
    });

};

var getCarnetAdresse = function(courriel, callback){

    var connexion = connecterLaBD();
    var query = 'SELECT * FROM CarnetAdresse WHERE courriel="'+courriel+'"';

    connexion.query(query, function(err, rows, fields){

        if(err) throw err;
        if(rows.length < 1){
            throw err;
        }
        else{
            var carnet = [];
            rows.forEach(function(entry) {
                carnet.push(entry.adresse);
            });

            callback(carnet);
        }

        connexion.end();
    });

};

//Met une nouvelle adresse par defaut a un user selon son ID
var changerAdresseDefaut = function(ID, adresse, callback){
	getInfo(ID, function(user){
		var connexion = connecterLaBD();
		var post = {adresse: user.adresse, courriel: user.courriel};
		connexion.query('INSERT INTO CarnetAdresse SET ?', post , function(err, rows, fields){
			if(err) throw err;
			var connexion = connecterLaBD();
			modifierUser(user.courriel, adresse, user.telephone, user.motDePasse, function(){
				var connexion = connecterLaBD();
				connexion.query("DELETE FROM CarnetAdresse WHERE adresse="+"'"+adresse+"'", function(err, rows, fields)	{
					if(err) throw err;
					connexion.end();
		
				});	
			});
		});
		
	});	
	
};



exports.getInfo = getInfo;
exports.modifierUser = modifierUser;
exports.inscrireUser = inscrireUser;
exports.supprimerUser = supprimerUser;
exports.ajouterMenu = ajouterMenu;
exports.modifierMenu = modifierMenu;
exports.supprimerMenu = supprimerMenu;
exports.getMenu = getMenu;
exports.ajouterRestaurant = ajouterRestaurant;
exports.modifierRestaurant = modifierRestaurant;
exports.supprimerRestaurant = supprimerRestaurant;
exports.getRestaurant = getRestaurant;
exports.getRestaurantID = getRestaurantID;
exports.getListeRestaurant = getListeRestaurant;
exports.getListeRestaurateur = getListeRestaurateur;
exports.ajouterCommande = ajouterCommande;
exports.getCommande = getCommande;
exports.changerStatusCommande = changerStatusCommande;
exports.supprimerCommande = supprimerCommande;
exports.getCommandePasPrete = getCommandePasPrete;
exports.getCommandePrete = getCommandePrete;
exports.getCommandePasPretePourListeRestaurant = getCommandePasPretePourListeRestaurant;
exports.etampeCommande = etampeCommande;
exports.getNomMenu = getNomMenu;
exports.inscrireLivraison = inscrireLivraison;
exports.trouverLivraison = trouverLivraison;
exports.trouverLivraisonPourLivreur = trouverLivraisonPourLivreur;
exports.getCarnetAdresse = getCarnetAdresse;
exports.changerAdresseDefaut = changerAdresseDefaut;
