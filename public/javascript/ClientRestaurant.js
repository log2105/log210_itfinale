/**
 * ClientRestaurant
 */
function ClientRestaurant(){
		
		this.restaurantEnCreation = null;
		
		this.creerRestaurant = function(nom, adresse, telephone, callback){
			
			unRestaurant = new restaurant(nom, adresse, telephone, '')
			
			this.restaurantEnCreation = unRestaurant         
			
			callback();
		};
		

		this.ajouterRestaurateur = function(ID, callback){
			this.restaurantEnCreation.setRestaurateur(ID);
			if(this.restaurantEnCreation.restaurateur == ''){
				alert('Aucun restaurateur sera associé à ce restaurant');
			};
			$.ajax({
				type: 'POST',
				data: JSON.stringify(this.restaurantEnCreation),
				contentType: 'application/json',
				url: '/admin/ajouterRestaurant',
				success: function(data) {
					console.log(JSON.stringify(data));
					callback(data);
				}
			});
			
		}
		
		this.getListeRestaurant = function(callback){
			$.get('/admin/getListeRestaurants', function(data) {
				result = data;
				callback(result);             
			});
		}

		this.getListeRestaurateurs = function(callback){
			$.get('/admin/getListeRestaurateurs', function(data) {				
				result = data;
				callback(result);             
			});
		}
		
		this.getRestaurant = function(ID, callback){
			$.ajax({
			    type: 'POST',
			    data: {'ID':ID},
			    url: '/admin/getRestaurant/'
				}).done(function(data) {
					callback(data);
			});
		}
		
		
}



function restaurant(nom, adresse, telephone){
	this.nom = nom;
	this.adresse = adresse;
	this.telephone = telephone;
	this.restaurateur = '';
	this.setRestaurateur = function(unRestaurateur){
		this.restaurateur = unRestaurateur;
	};
	
};
