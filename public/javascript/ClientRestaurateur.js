/**
 * ClientRestaurateur
 */
function ClientRestaurateur(){
		
		this.restaurateurEnCreation = null;
		this.listeRestaurants = [];
		
		this.creerRestaurateur = function(prenom, nom, dateNaissance, adresse, telephone, courriel, motDePasse, callback){
			
			unRestaurateur = new restaurateur(prenom, nom, dateNaissance, adresse, telephone, courriel, motDePasse);

			this.restaurateurEnCreation = unRestaurateur;    
			
			callback();
		};
		

		this.ajouterRestaurateur = function(callback){
			if(jQuery.isEmptyObject(this.listeRestaurants)){
				alert('Aucun restaurants sera associé à ce restaurateur');
			};
			$.ajax({
				type: 'POST',
				data: JSON.stringify(this.restaurateurEnCreation),
				contentType: 'application/json',
				url: '/admin/ajouterRestaurateur',
				success: function(data) {
					console.log(JSON.stringify(data));
					callback(data);
				}
			});

			if(this.listeRestaurants != []){
				$.each(this.listeRestaurants, function(){
					$.ajax({
					type: 'POST',
					data: JSON.stringify(this),
					contentType: 'application/json',
					url: '/admin/setRestaurateur',
					success: function(data) {
							console.log(JSON.stringify(data));
						}
					});
				});
			};
		}
		
		this.getListeRestaurant = function(callback){
			$.get('/admin/getListeRestaurants', function(data) {
				result = data;
				callback(result);             
			});
		}

		this.getListeRestaurateurs = function(callback){
			$.get('/admin/getListeRestaurateurs', function(data) {				
				result = data;
				callback(result);             
			});
		}
		
		this.getRestaurant = function(ID, callback){
			$.get('/admin/getRestaurant', function(data) {
				result = data;
				callback(result);             
			});
		}

		this.setRestaurant = function(ID){
			if(this.restaurateurEnCreation!=null)
			    this.listeRestaurants.push({'ID':ID, 'Restaurateur':this.restaurateurEnCreation.courriel});
			console.log(this.listeRestaurants);
		}
		
		this.getRestaurateur = function(ID, callback){
			$.ajax({
			    type: 'POST',
			    data: {'ID':ID},
			    url: '/admin/getRestaurateur/'
				}).done(function(data) {
					callback(data);
			});
		}
		
}



function restaurateur(prenom, nom, dateNaissance, adresse, telephone, courriel, motDePasse){
	this.prenom = prenom;	
	this.name = nom;
	this.dateNaissance = dateNaissance;
	this.adresse = adresse;
	this.numeroTelephone = telephone;
	this.courriel = courriel;
        this.password = motDePasse;
        this.type = 'restaurateur';
};
