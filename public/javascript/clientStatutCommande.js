var clientCommande = new ClientCommande();
var listeCommandes;

$(document).ready(function() {
    $('#commandeList table tbody').on('click', 'td a.linkmodifierstatutPrep', setStatutPrep);
    $('#commandeList table tbody').on('click', 'td a.linkmodifierstatutPret', setStatutPret);
    afficherCommandes();
});



function afficherCommandes() {
    var tableContent = '';    

    clientCommande.getListeCommandePasPretePourRestaurateur(function(data){
	listeCommandes = data;
    
        $.each(listeCommandes, function(){
            tableContent += '<tr>';
            tableContent += '<td>'+this.ID + '</td>';
            tableContent += '<td>' + this.restaurant + '</td>';
            tableContent += '<td>' + this.client + '</td>';
            tableContent += '<td>' + this.plats + '</td>';
            tableContent += '<td>' + this.status + '</td>';
	    tableContent += '<td><a href="#" class="linkmodifierstatutPrep" rel="' + this.ID + '">En preparation</a></td>';
            tableContent += '<td><a href="#" class="linkmodifierstatutPret" rel="' + this.ID + '">Prete</a></td>';
            tableContent += '</tr>';
        });
        $('#commandeList table tbody').html(tableContent);
    });
};

function setStatutPrep(event) {
	clientCommande.setStatut($(this).attr('rel'), 'En preparation', function(){
		alert('Statut change.');
		afficherCommandes();
	});
}

function setStatutPret(event) {
	clientCommande.setStatut($(this).attr('rel'), 'Prête', function(){
		alert('Statut change.');
		afficherCommandes();
	});
}
