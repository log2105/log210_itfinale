var clientRestaurateur = new ClientRestaurateur();
var IDAModifier = '';
var listeRestaurateurs;

$(document).ready(function() {
    $('#restaurateursList table tbody').on('click', 'td a.linkdeleteresto', deleteResto);
    $('#restaurateursList table tbody').on('click', 'td a.linkmodifierresto', commencerModResto);
    $('#btnModInfoResto').on('click', modifierResto);
    afficherRestaurateurs();
});

function afficherRestaurateurs() {
	var tableContent = '';

    clientRestaurateur.getListeRestaurateurs(function(data){
	listeRestaurateurs = data;

        $.each(listeRestaurateurs, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.nom + '</td>';
            tableContent += '<td>' + this.courriel + '</td>';
	    tableContent += '<td><a href="#" class="linkmodifierresto" rel="' + this.courriel + '">Modifier</a></td>';
            tableContent += '<td><a href="#" class="linkdeleteresto" rel="' + this.user_id + '">Supprimer</a></td>';
            tableContent += '</tr>';
        });

        $('#restaurateursList table tbody').html(tableContent);
    });
};

function commencerModResto(event) {
    IDAModifier = $(this).attr('rel');
    alert('Restaurateur '+$(this).attr('rel')+' prêt à être modifié.');
    clientRestaurateur.getRestaurateur(IDAModifier, function(result){
        $('#adresseInput').val(result.adresse);
        $('#numeroTelephoneInput').val(result.telephone);
        $('#courrielInput').val(result.courriel);
        $('#passwordInput').val(result.motDePasse);
    });
        
}

function deleteResto(event) {
console.log($(this).attr('rel'));
    event.preventDefault();

    // dialog de confirmation
    var confirmation = confirm('Voulez-vous supprimer ce restaurateur?');

    // Si l'utilisateur a confirme
    if (confirmation === true) {

        $.ajax({
            type: 'POST',
	    data: {'ID':$(this).attr('rel')},
            url: '/admin/supprimerRestaurateur/'
        }).done(function( response ) {

            alert('Restaurateur supprimé.');

            // Update tableau
            afficherRestaurateurs();

        });

    }
    else {
        return false;
    }
};

function modifierResto(event) {

    event.preventDefault();

    var confirmation = confirm('Voulez-vous modifier le restaurant '+IDAModifier+'?');

    // Si l'utilisateur a confirme
    if (confirmation === true) {
		var unRestaurateur = { 
			'adresse': $('#restaurateurModification fieldset input#adresseInput').val(), 
			'numeroTelephone': $('#restaurateurModification fieldset input#numeroTelephoneInput').val(),
			'courriel': $('#restaurateurModification fieldset input#courrielInput').val(),
			'password': $('#restaurateurModification fieldset input#passwordInput').val(),	
		}
console.log(unRestaurateur);
        $.ajax({
            		type: 'POST',
            		data: unRestaurateur,
            		url: '/admin/modifierRestaurateur',
            		dataType: 'JSON'
        }).done(function( response ) {

            alert('Restaurateur modifié.');

            // Update tableau
            afficherRestaurateurs();

        });

    }
    else {
        return false;
    }
};
