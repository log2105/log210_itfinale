var clientRestaurant = new ClientRestaurant();
var clientMenu = new ClientMenu();
var idRestaurant;
var nomMenu;
var listeRestaurants;
var listePlats='';

$(document).ready(function() {
    afficherRestaurants();
    $('#restaurantsList table tbody').on('click', 'td a.linkregresto', regResto);
    $('#btnConfirmerNomMenu').on('click', enregistrerNom);
    $('#btnAjoutPlat').on('click', ajouterPlat);
    $('#btnConfirmerMenu').on('click', confirmerMenu);
});

function afficherRestaurants() {
    var tableContent = '';    

    clientRestaurant.getListeRestaurant(function(data){
	listeRestaurants = data;
    
        $.each(listeRestaurants, function(){
            tableContent += '<tr>';
            tableContent += '<td>'+this.nom + '</td>';
	    tableContent += '<td><a href="#" class="linkregresto" rel="' + this.ID + '">Lui ajouter un menu</a></td>';
            tableContent += '</tr>';
        });
        $('#restaurantsList table tbody').html(tableContent);
    });
};

function ajouterPlat(){
    var nomPlat = $('input#nomInput').val();
    var descriptionPlat = $('input#descriptionInput').val();
    var prixPlat = $('input#prixInput').val();
    if(prixPlat=='' || nomPlat==''){
        alert('Un prix et un nom sont obligatoire pour le plat');
    }else if(descriptionPlat==''){
        // dialog de confirmation
        var confirmation = confirm('Ce plat sera enregistre sans description.');

        // Si l'utilisateur a confirme
        if (confirmation === true) {
            mettrePlat(nomPlat, descriptionPlat, prixPlat);
        }

    }else{
        mettrePlat(nomPlat, descriptionPlat, prixPlat);
    }
}

function mettrePlat(nomPlat, descriptionPlat, prixPlat){
	    clientMenu.ajouterPlat(nomPlat, descriptionPlat, prixPlat);
	    alert('Plat ajoute');
	    $('#nomInput').val('');
	    $('#descriptionInput').val('');
	    $('#prixInput').val('');
            listePlats += '<tr>';
            listePlats += '<td>'+nomPlat + '</td>';
	    listePlats += '<td>' + descriptionPlat + '</td>';
	    listePlats += '<td>' + prixPlat + '</td>';
            listePlats += '</tr>';
	    $('#platsList table tbody').html(listePlats);
}

function enregistrerNom(){
    nomMenu = $('input#nomMenu').val();
    clientMenu.creerMenu(nomMenu);
}

function regResto(event){
    idResto = $(this).attr('rel');
    clientMenu.ajouterRestaurant(idResto);
    $('#nomResto').val(idResto);
}

function confirmerMenu(){
    if(idResto=='' || nomMenu==''){
        alert('Un nom de menu et un restaurant associe sont obligatoires');
    } else{
        clientMenu.confirmerMenu(function(){
            alert('Menu ajoute');
        });
    }
}
