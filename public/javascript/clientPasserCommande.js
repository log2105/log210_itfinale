var clientMenu = new ClientMenu();
var clientRestaurant = new ClientRestaurant();
var clientCommande = new ClientCommande();
var fabriqueCommande = FabriqueCommande;
var fabriqueUtilisateur = FabriqueUtilisateur;
var commandeCourante;
var client;
var restaurant;
var menu;
var listeRestaurants;
var adresse;
var listePlats;

$(document).ready(function() {
	var clientTemp;
	var carnet;
	
	$.post('/client/getClient', function(data) {				
		clientTemp = data;
		clientCommande.getCarnetAdresse(clientTemp.courriel, function(data){
			carnet = data;
console.log(carnet);
			client = fabriqueUtilisateur.creerUtilisateur(clientTemp.nom, clientTemp.dateNaissance, clientTemp.adresse, clientTemp.telephone, clientTemp.courriel, clientTemp.motDePasse, clientTemp.type, carnet);
			console.log(client);
			afficherRestaurants();
		});      
	});
		
	
	
    $('#tablePrincipale table tbody').on('click', 'td a.linkchoixresto', debuterCommande);
	$('#tablePrincipale table tbody').on('click', 'td a.linkajouterplat', ajouterPlat);
    $('#btnConfirmerCommande').on('click', afficherAdresse);
});

function afficherRestaurants(){
    var tableContent = '';    
    
	tableContent += '<th>Restaurant</th>';
	$('#tablePrincipale table thead').html(tableContent);
	
	tableContent = '';
	
    clientRestaurant.getListeRestaurant(function(data){
	listeRestaurants = data;
    
        $.each(listeRestaurants, function(){
            tableContent += '<tr>';
            tableContent += '<td>'+this.nom + '</td>';
	    tableContent += '<td><a href="#" class="linkchoixresto" rel="' + this.ID + '">Choisir</a></td>';
            tableContent += '</tr>';
        });
        $('#tablePrincipale table tbody').html(tableContent);
    });
}

function debuterCommande(event){
	    restaurant = $(this).attr('rel');  
    clientMenu.getNomMenu(restaurant, function(data){

		menu = data[0];
		commandeCourante = fabriqueCommande.creerCommandeVide(restaurant, client.courriel, menu);
console.log(commandeCourante);
		afficherPlats();
    });
}

function afficherPlats(){
    var tableContent = '';
    listePlats = [];
    var i = 0;

	tableContent += '<th>Plat</th>';
	tableContent += '<th>Description</th>';
	tableContent += '<th>Prix</th>';
	$('#tablePrincipale table thead').html(tableContent);
	
	tableContent = '';
	
    clientMenu.getMenu(restaurant, menu, function(data){
		listeRestaurants = data.plat;
		
        $.each(listeRestaurants, function(){
            listePlats.push(this);
			tableContent += '<tr>';
            tableContent += '<td>'+this.nom + '</td>';
			tableContent += '<td>'+this.description + '</td>';
            tableContent += '<td>'+this.prix + '</td>';
			tableContent += '<td><a href="#" class="linkajouterplat" rel="' + i + '">Ajouter</a></td>';
            tableContent += '</tr>';
	    i += 1;
        });
        $('#tablePrincipale table tbody').html(tableContent);
    });
}

function creerCommande(restaurant, client, menu){
	commandeCourante = fabriqueCommande(restaurant, client, menu);
}

function ajouterPlat(event){
	var plat = listePlats[$(this).attr('rel')];
	var quantite = prompt("Entrez la quantite.", "1");
console.log(plat);
	commandeCourante.ajouterPlat(plat, quantite);
	afficherCommande();
}

function afficherAdresse(){
	var adresse;
	var r = confirm("Livrez a cette adresse: "+client.adresse+"?");
	if (r == true) {
		adresse = client.adresse;
	} else {
		adresse = prompt("Choisir parmis votre carnet d'adresse ou entrez une nouvelle adresse: "+client.carnetAdresse,'');
	}
	ajouterAdresse(adresse);
	confirmerCommande();
}

function ajouterAdresse(adresse){
	if(adresse!=client.adresse){
		client.changerAdresseParDefaut(adresse, client.courriel);
	}
	
	commandeCourante.setAdresse(adresse);
}

function confirmerCommande(){
	var dateLivraison = prompt('Inscrire une date: ', "");
	commandeCourante.setDate(dateLivraison);
console.log(commandeCourante);
	alert('Votre total est: '+ commandeCourante.getTotal());
	var r = confirm("Finaliser la commande?");
	if (r == true) {

		 clientCommande.inscrireCommande(commandeCourante, function(){
			alert('Votre commande est passe.');
			window.location.href = '/client/payerCommande';
		});	
	} else {
	}
}

function afficherCommande(){
	var tableContent = '';

	$.each(commandeCourante.plats, function(){
        tableContent += '<tr>';
        tableContent += '<td>'+this.plat.nom + '</td>';
		tableContent += '<td>'+this.quantite + '</td>';
		tableContent += '<td>'+this.getTotal() + '</td>';
        tableContent += '</tr>';
 	});
	
        $('#tableCommande table tbody').html(tableContent);
}
