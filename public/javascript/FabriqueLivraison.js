/**
 * Created by Frank on 2015-03-21.
 */

var FabriqueLivraison = {

    creerLivraison : function(uneCommande, livreur){

        //on determine la date courante
        //code emprunté de Obsidian sur stackoverflow
        //source: http://stackoverflow.com/questions/10599148/how-do-i-get-the-current-time-only-in-javascript

        var date = new Date(),
            jour = date.getDate()
        mois = date.getMonth();
        annee = date.getFullYear()
        heure = date.getHours(),
            minute = date.getMinutes();

        dateComplete = ""+jour+"-"+mois+"-"+annee+" "+heure+":"+minute
        //fin du code emprunté

        var livraison = {
            ID: "",
            commande: uneCommande.ID,
            livreur:livreur,
            dateDepart:dateComplete

        }
        return livraison
    },
    creerLivraisonAvecDate : function(ID, commande, livreur, date){

        var livraison = {
            ID: ID,
            commande: commande,
            livreur:livreur,
            dateDepart:date

        }
        return livraison
    }


}

exports.FabriqueLivraison = FabriqueLivraison;