/**
 * Created by Frank on 2015-03-03.
 */

var FabriqueRestaurant = {

    creerRestaurantComplet: function (nom, adresse, telephone, restaurateur, ID) {

        var restaurant = {
            nom: nom,
            adresse: adresse,
            telephone: telephone,
            ID: ID,
            restaurateur: restaurateur,
            setRestaurateur: function (unRestaurateur) {
                this.restaurateur = unRestaurateur;
            }


        }
        return restaurant
    },
    creerRestaurant: function (nom, adresse, telephone){

        var restaurant = {
            nom: nom,
            adresse: adresse,
            telephone: telephone,
            ID: "",
            restaurateur: "",
            setRestaurateur: function (unRestaurateur) {
                this.restaurateur = unRestaurateur;
            }


        }
        return restaurant
    },
    creerRestaurantSansID: function (nom, adresse, telephone, ID) {

        var restaurant = {
            nom: nom,
            adresse: adresse,
            telephone: telephone,
            ID: "",
            restaurateur: restaurateur,
            setRestaurateur: function (unRestaurateur) {
                this.restaurateur = unRestaurateur;
            }


        }
        return restaurant
    }
}

exports.FabriqueRestaurant = FabriqueRestaurant;